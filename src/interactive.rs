use std::io::{self, BufRead, Write};

use crate::io::Streams;
use crate::database::Database;

fn read_line<I: BufRead>(mut r#in: I) -> io::Result<String> {
    let mut pre_n = String::new();
    r#in.read_line(&mut pre_n)?;
    Ok(pre_n)
}

pub fn ask<D: Database, I: BufRead, O: Write, E: Write>(streams: &mut Streams<D, I, O, E>, question: &str) -> io::Result<bool> {
    write!(streams.out, "{} [y/N] ", question)?;
    streams.out.flush()?;

    Ok(read_line(&mut streams.r#in)?.to_lowercase().starts_with('y'))
}
