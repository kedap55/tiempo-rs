use std::convert::TryFrom;
use std::io::{BufRead, Write};

use clap::ArgMatches;
use chrono::{DateTime, Utc};

use crate::database::Database;
use crate::error::{Error, Result};
use crate::editor;
use crate::commands::{Command, Facts};
use crate::timeparse::parse_time;
use crate::old::{time_or_warning, warn_if_needed};
use crate::io::Streams;

#[derive(Default)]
pub struct Args {
    pub at: Option<DateTime<Utc>>,
    pub note: Option<String>,
}

impl<'a> TryFrom<&'a ArgMatches<'a>> for Args {
    type Error = Error;

    fn try_from(matches: &'a ArgMatches) -> Result<Self> {
        Ok(Args {
            at: matches.value_of("at").map(|s| parse_time(s)).transpose()?,
            note: matches.value_of("note").map(|s| s.into()),
        })
    }
}

pub struct InCommand {}

impl<'a> Command<'a> for InCommand {
    type Args = Args;

    fn handle<D, I, O, E>(args: Args, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let start = args.at.unwrap_or(facts.now);
        let sheet = streams.db.current_sheet()?.unwrap_or_else(|| "default".into());

        if streams.db.running_entry(&sheet)?.is_some() {
            writeln!(streams.out, "Timer is already running for sheet '{}'", sheet)?;

            return Ok(());
        }

        let note = if let Some(note) = args.note {
            Some(note.trim().to_owned())
        } else if !facts.config.require_note {
            None
        } else {
            Some(editor::get_string(facts.config.note_editor.as_deref(), None)?)
        };

        let (start, needs_warning) = time_or_warning(start, &streams.db)?;

        streams.db.entry_insert(start, None, note, &sheet)?;

        writeln!(streams.out, "Checked into sheet \"{}\".", sheet)?;

        warn_if_needed(&mut streams.err, needs_warning, &facts.env)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;
    use chrono::{TimeZone, Local};

    use crate::test_utils::Ps;
    use crate::database::SqliteDatabase;
    use crate::config::Config;

    use super::*;

    #[test]
    fn handles_new_entry() {
        let args = Args {
            at: None,
            note: Some("hola".into()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 0);

        InCommand::handle(args, &mut streams, &facts).unwrap();

        let e = streams.db.entries_full(None, None).unwrap().into_iter().next().unwrap();

        assert_eq!(e.note, Some("hola".into()));
        assert_eq!(e.start, facts.now);
        assert_eq!(e.end, None);
        assert_eq!(e.sheet, "default".to_owned());

        assert_eq!(Ps(&String::from_utf8_lossy(&streams.out)), Ps("Checked into sheet \"default\".\n"));
        assert_eq!(Ps(&String::from_utf8_lossy(&streams.err)), Ps(""));
    }

    #[test]
    fn test_handles_already_running_entry() {
        let args = Args {
            at: None,
            note: Some("hola".into()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        streams.db.entry_insert(facts.now, None, None, "default".into()).unwrap();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 1);

        InCommand::handle(args, &mut streams, &facts).unwrap();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 1);

        assert_eq!(
            Ps(&String::from_utf8_lossy(&streams.out)),
            Ps("Timer is already running for sheet 'default'\n")
        );
    }

    #[test]
    fn no_note_and_no_mandatory_leaves_none() {
        let args = Default::default();
        let mut streams = Streams::fake(b"");
        let facts = Facts::new().with_config(Config {
            require_note: false,
            ..Default::default()
        });

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 0);

        InCommand::handle(args, &mut streams, &facts).unwrap();

        let e = streams.db.entries_full(None, None).unwrap().into_iter().next().unwrap();

        assert_eq!(e.note, None);
        assert_eq!(e.start, facts.now);
        assert_eq!(e.end, None);
        assert_eq!(e.sheet, "default".to_owned());
    }

    #[test]
    fn warns_if_old_database() {
        let args = Args {
            at: None,
            note: Some("hola".into()),
        };
        let mut streams = Streams::fake(b"").with_db({
            let mut db = SqliteDatabase::from_memory().unwrap();

            db.init_old().unwrap();

            db
        });
        let facts = Facts::new();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 0);

        InCommand::handle(args, &mut streams, &facts).unwrap();

        let e = streams.db.entries_full(None, None).unwrap().into_iter().next().unwrap();

        assert_eq!(e.note, Some("hola".into()));
        assert_eq!(e.start, Utc.from_utc_datetime(&facts.now.with_timezone(&Local).naive_local()));
        assert_eq!(e.end, None);
        assert_eq!(e.sheet, "default".to_owned());

        assert_eq!(Ps(&String::from_utf8_lossy(&streams.out)), Ps("Checked into sheet \"default\".\n"));
        assert_eq!(Ps(&String::from_utf8_lossy(&streams.err)), Ps(
            "[WARNING] You are using the old timetrap format, it is advised that \
            you update your database using t migrate\n"));
    }

    #[test]
    fn notes_are_trimmed() {
        let args = Args {
            at: None,
            note: Some("hola\n".into()),
        };
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 0);

        InCommand::handle(args, &mut streams, &facts).unwrap();

        let e = streams.db.entries_full(None, None).unwrap().into_iter().next().unwrap();

        assert_eq!(e.note, Some("hola".into()));
        assert_eq!(e.start, facts.now);
        assert_eq!(e.end, None);
        assert_eq!(e.sheet, "default".to_owned());

        assert_eq!(Ps(&String::from_utf8_lossy(&streams.out)), Ps("Checked into sheet \"default\".\n"));
        assert_eq!(Ps(&String::from_utf8_lossy(&streams.err)), Ps(""));
    }
}
