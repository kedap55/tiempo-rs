use std::convert::TryFrom;
use std::io::{BufRead, Write};

use clap::ArgMatches;
use chrono::{DateTime, Utc, Local};
use regex::Regex;

use crate::error::{Result, Error};
use crate::database::Database;
use crate::formatters::Formatter;
use crate::timeparse::parse_time;
use crate::regex::parse_regex;
use crate::io::Streams;

use super::{Command, Facts, display::{Sheet, entries_for_display}};

#[derive(Default)]
pub struct Args {
    ids: bool,
    end: Option<DateTime<Utc>>,
    format: Option<Formatter>,
    grep: Option<Regex>,
    sheet: Option<Sheet>,
}

impl<'a> TryFrom<&'a ArgMatches<'a>> for Args {
    type Error = Error;

    fn try_from(matches: &'a ArgMatches) -> Result<Args> {
        Ok(Args {
            ids: matches.is_present("ids"),
            end: matches.value_of("end").map(|s| parse_time(s)).transpose()?,
            format: matches.value_of("format").map(|v| v.parse()).transpose()?,
            grep: matches.value_of("grep").map(parse_regex).transpose()?,
            sheet: matches.value_of("sheet").map(|s| s.parse()).transpose()?,
        })
    }
}

pub struct TodayCommand { }

impl<'a> Command<'a> for TodayCommand {
    type Args = Args;

    fn handle<D, I, O, E>(args: Self::Args, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let start = Some(facts.now.with_timezone(&Local).date().and_hms(0, 0, 0).with_timezone(&Utc));

        entries_for_display(
            start,
            args.end,
            args.sheet,
            streams,
            args.format.unwrap_or_else(|| facts.config.default_formatter.clone()),
            args.ids,
            args.grep,
            facts
        )
    }
}

#[cfg(test)]
mod tests {
    use chrono::TimeZone;

    use crate::config::Config;

    use super::*;

    #[test]
    fn respect_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Default::default();
        let mut streams = Streams::fake(b"");
        let facts = Facts::new().with_config(Config {
            default_formatter: Formatter::Ids,
            ..Default::default()
        }).with_now(Utc.ymd(2021, 6, 30).and_hms(11, 0, 0));

        streams.db.entry_insert(Utc.ymd(2021, 6, 30).and_hms(10, 0, 0), None, Some("hola".into()), "default".into()).unwrap();
        streams.db.entry_insert(Utc.ymd(2021, 6, 30).and_hms(10, 10, 0), None, Some("hola".into()), "default".into()).unwrap();

        TodayCommand::handle(args, &mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }
}
