use std::convert::TryFrom;
use std::io::{BufRead, Write};

use clap::ArgMatches;
use chrono::{DateTime, Utc, Local, Duration, Weekday, Datelike};
use regex::Regex;

use crate::error::{Result, Error};
use crate::database::Database;
use crate::formatters::Formatter;
use crate::config::WeekDay;
use crate::regex::parse_regex;
use crate::timeparse::parse_time;
use crate::io::Streams;

use super::{Command, Facts, display::{Sheet, entries_for_display}};

trait AsNum {
    fn as_num(&self) -> i64;
}

impl AsNum for WeekDay {
    fn as_num(&self) -> i64 {
        match &self {
            WeekDay::Monday => 1,
            WeekDay::Tuesday => 2,
            WeekDay::Wednesday => 3,
            WeekDay::Thursday => 4,
            WeekDay::Friday => 5,
            WeekDay::Saturday => 6,
            WeekDay::Sunday => 7,
        }
    }
}

impl AsNum for Weekday {
    fn as_num(&self) -> i64 {
        match &self {
            Weekday::Mon => 1,
            Weekday::Tue => 2,
            Weekday::Wed => 3,
            Weekday::Thu => 4,
            Weekday::Fri => 5,
            Weekday::Sat => 6,
            Weekday::Sun => 7,
        }
    }
}

/// Given a local datetime, returns the time of the previous week_start's start
fn prev_day(now: DateTime<Local>, week_start: WeekDay) -> DateTime<Utc> {
    let begining = match now.weekday().as_num() - week_start.as_num() {
        num if num == 0 => now,
        num if num > 0 => now - Duration::days(num),
        num if num < 0 => now - Duration::days(7 - num.abs()),
        _ => unreachable!(),
    };

    begining.date().and_hms(0, 0, 0).with_timezone(&Utc)
}

#[derive(Default)]
pub struct Args {
    ids: bool,
    end: Option<DateTime<Utc>>,
    format: Option<Formatter>,
    grep: Option<Regex>,
    sheet: Option<Sheet>,
}

impl<'a> TryFrom<&'a ArgMatches<'a>> for Args {
    type Error = Error;

    fn try_from(matches: &'a ArgMatches) -> Result<Args> {
        Ok(Args {
            ids: matches.is_present("ids"),
            end: matches.value_of("end").map(|s| parse_time(s)).transpose()?,
            format: matches.value_of("format").map(|v| v.parse()).transpose()?,
            grep: matches.value_of("grep").map(parse_regex).transpose()?,
            sheet: matches.value_of("sheet").map(|s| s.parse()).transpose()?,
        })
    }
}

pub struct WeekCommand { }

impl<'a> Command<'a> for WeekCommand {
    type Args = Args;

    fn handle<D, I, O, E>(args: Self::Args, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let start = prev_day(facts.now.with_timezone(&Local), facts.config.week_start);

        entries_for_display(
            Some(start),
            args.end,
            args.sheet,
            streams,
            args.format.unwrap_or_else(|| facts.config.default_formatter.clone()),
            args.ids,
            args.grep,
            facts
        )
    }
}

#[cfg(test)]
mod tests {
    use chrono::TimeZone;

    use crate::config::Config;

    use super::*;

    #[test]
    fn test_prev_day() {
        // starting a saturday
        let now = Local.ymd(2021, 7, 10).and_hms(18, 31, 0);

        assert_eq!(prev_day(now, WeekDay::Monday), Local.ymd(2021, 7, 5).and_hms(0, 0, 0).with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Tuesday), Local.ymd(2021, 7, 6).and_hms(0, 0, 0).with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Wednesday), Local.ymd(2021, 7, 7).and_hms(0, 0, 0).with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Thursday), Local.ymd(2021, 7, 8).and_hms(0, 0, 0).with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Friday), Local.ymd(2021, 7, 9).and_hms(0, 0, 0).with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Saturday), Local.ymd(2021, 7, 10).and_hms(0, 0, 0).with_timezone(&Utc));
        assert_eq!(prev_day(now, WeekDay::Sunday), Local.ymd(2021, 7, 4).and_hms(0, 0, 0).with_timezone(&Utc));
    }

    #[test]
    fn respect_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Default::default();
        let mut streams = Streams::fake(b"");
        let now = Utc.ymd(2021, 7, 1).and_hms(10, 0, 0);
        let facts = Facts::new().with_config(Config {
            default_formatter: Formatter::Ids,
            ..Default::default()
        }).with_now(now);

        streams.db.entry_insert(Utc.ymd(2021, 6, 30).and_hms(10, 0, 0), None, Some("hola".into()), "default".into()).unwrap();
        streams.db.entry_insert(Utc.ymd(2021, 6, 30).and_hms(10, 10, 0), None, Some("hola".into()), "default".into()).unwrap();

        WeekCommand::handle(args, &mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }
}
